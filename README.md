# That's Enough
That's Enough is a chrome extension that helps you focus on whatever you're doing by blocking distracting sites. It does this in a few ways:

1. **Focus Mode**<br>
   Focus Mode is a mode that you can enable to disable all sites on your block list. This means you cannot visit them at all.
1. **Timeout**<br>
   Timeout is the default mode, where every site is limited to 5 minutes at a time. When the time runs out, you won't be able to visit the site for 1 hour. All of these durations are configurable.


## Links
- **[Time Well Spent](https://timewellspent.io)**: This site originally inspired me to make this extension.

## License
This software is licensed under the MIT license, of which you can find a copy in this repository.
