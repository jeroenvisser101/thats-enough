let blockPage = chrome.runtime.getURL('/popup.html')

let serialize = (obj) => btoa(JSON.stringify(obj))

let processRules = (rules) => {
  return rules.map(rule => {
    let regex = new RegExp(`^https?:\\/\\/(www\\.)?${rule.domain}`, 'i')
    return Object.assign(rule, {regex})
  })
}

let rules = processRules([
  {name: 'Facebook', domain: 'facebook.com'},
  {name: 'NOS.nl', domain: 'nos.nl'},
  {name: 'dumpert.nl', domain: 'dumpert.nl'},
  {name: 'Tweakers.net', domain: 'tweakers.net'},
  {name: 'AT5', domain: 'at5.nl'},
  {name: 'Instagram', domain: 'instagram.com'},
  {name: 'YouTube', domain: 'youtube.com'}
])

chrome.webRequest.onBeforeRequest.addListener((details) => {
  let rule = rules.find(rule => details.url.match(rule.regex))
  if (rule) {
    return {redirectUrl: `${blockPage}?details=${serialize({rule, details})}`}
  }
}, {urls: ['<all_urls>']}, ['blocking'])
